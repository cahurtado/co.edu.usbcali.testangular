import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../usuario.service';
import { Usuario } from '../usuario';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {

  public titulo:string;
  public listaUsuario:Usuario[];

  constructor(public usuarioService:UsuarioService) { 
    console.log('Se ejecutó el constructor');
  }

  ngOnInit(): void {
    console.log('Se ejecutó en ngOnInit');
    this.titulo = 'Lista de Usuarios';
    this.findAll();
  }

  /**
   * findAll
   */
  public findAll():void {
    this.listaUsuario = this.usuarioService.findAll();
  }

  /**
   * consultar
   */
  public consultar(usuario:Usuario):void {
    console.table(usuario);
  }
}
